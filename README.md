# Double commander as root

Run Double commander with admin privileias (as root). Patched version for run by menu, search and rofi.

Usage: `doublecmd-root`

## INSTALLATION

### From the [AUR](https://aur.archlinux.org/packages/doublecmd-admin/):
```
git clone https://aur.archlinux.org/doublecmd-admin.git
cd doublecmd-admin/
makepkg -sci
```

### Install from source:
Unpack the source package and run command (like root):
```
make install
```

### Uninstall from source:
Run command (like root):
```
make uninstall
```

### Build own ArchLinux package
You need these packages for building the ArchLinux package:
```
base-devel git wget yajl doublecmd
```

Run command:
```
make build-arch
```

### Install from *.pkg.tar.xz package:
Run command (like root):
```
pacman -U distrib/doublecmd-admin*.pkg.tar.xz
```
